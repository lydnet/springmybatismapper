package com.darksider.service;

import com.darksider.model.User;

import java.util.List;

/**
 * IUserService
 *
 * @author Darksiderl
 * @date 2016/9/21
 */
public interface IUserService {
    public User findUserById(int id);
    public void addUser(User user);
    List<User> getAllUsers();
}
