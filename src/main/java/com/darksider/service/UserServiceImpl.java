package com.darksider.service;

import com.darksider.dao.IUserDao;
import com.darksider.dao.UserDaoImpl;
import com.darksider.model.User;

import java.util.List;

/**
 * UserServiceImpl
 *
 * @author Darksiderl
 * @date 2016/9/21
 */
public class UserServiceImpl implements IUserService {

    private IUserDao userDao;

    public UserServiceImpl() {
        userDao = new UserDaoImpl();
    }

    public User findUserById(int id) {
        return userDao.findUserById(id);
    }
    public void addUser(User user){
        userDao.addUser(user);
    }

    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

}
