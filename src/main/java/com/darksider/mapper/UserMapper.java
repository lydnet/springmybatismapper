package com.darksider.mapper;

import com.darksider.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * UserMapper
 *
 * @author Darksiderl
 * @date 2016/9/21
 */
public interface UserMapper {
    @Select("select * from user_nothing where id = #{id}")
    User findUserById(int id);

    @Insert("insert into user_nothing(username,password) values(#{username},#{password})")
    void addUser(User user);


    @Select("select * from user_nothing")
    List<User> getAllUsers();
}
