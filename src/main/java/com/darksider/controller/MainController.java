package com.darksider.controller;

import com.darksider.model.User;
import com.darksider.service.IUserService;
import com.darksider.service.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * MainController
 *
 * @author Darksiderl
 * @date 2016/9/21
 */
@Controller
public class MainController {

    private IUserService service = new UserServiceImpl();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(User user) {
        return "index";
    }


//    @RequestMapping(value="/",method = RequestMethod.GET)
//    public  String nice(Model model){
//        model.addAttribute("result","hahaha 第一个参数");
//        return  "nice";
//    }

    @RequestMapping(value="nice",method = RequestMethod.GET)
    @ResponseBody
    public List<User> nice(Model model){
        return service.getAllUsers();
    }

    @RequestMapping(value ="/toJson",method=RequestMethod.POST)
    @ResponseBody
    public User toJson(User user){
        service.addUser(user); //一起测试了
        return service.findUserById(3);
    }


}
