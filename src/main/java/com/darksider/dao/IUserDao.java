package com.darksider.dao;

import com.darksider.model.User;

import java.util.List;

/**
 * IUserDao
 *
 * @author Darksiderl
 * @date 2016/9/21
 */
public interface IUserDao {
    public User findUserById(int id); //查询
    public void addUser(User user); //添加
    List<User> getAllUsers(); //获取所有用户
}
