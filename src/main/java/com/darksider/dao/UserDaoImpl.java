package com.darksider.dao;

import com.darksider.mapper.UserMapper;
import com.darksider.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

/**
 * UserDaoImpl
 *
 * @author Darksiderl
 * @date 2016/9/21
 */
public class UserDaoImpl implements IUserDao{
    private SqlSessionFactory sessionFactory;
    private SqlSession session;
    private UserMapper mapper;
    public UserDaoImpl() {
        String resource = "conf.xml";
        try {
            Reader reader = Resources.getResourceAsReader(resource);
            sessionFactory = new SqlSessionFactoryBuilder().build(reader);
            session = sessionFactory.openSession();
            mapper = session.getMapper(UserMapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public User findUserById(int id) {
//        String statement = "userMapper.findUserById";
//        User user = (User)session.selectOne(statement, id);
//        return user;
        return mapper.findUserById(id);
    }
    public void addUser(User user) {
//        String statement = "userMapper.addUser";
//        session.insert(statement, user);
//        session.commit();  //一定要记得commit
        mapper.addUser(user);
        session.commit();
    }

    public List<User> getAllUsers() {
        return mapper.getAllUsers();
    }
}